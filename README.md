Introduction
============

La tache de création d'un résumé d'un texte consiste de la création d'un
texte court et cohérent qui garde les informations les plus importants
du texte original. Historiquement cette tache était faite avec une
approche extractive où les phrases les plus pertinentes sont prises du
texte originale. Dans ce projet, nous nous concentrons sur cette
approche (nous la présenterons plus en détails ainsi que l'approche dite
\"abstractive\" dans la partie \"État de l'art\") et ses enjeux.

Il existe plusieurs difficultés lors de l'automatisation de cette tache.
En première temps, comme dans beaucoup de taches NLP, il y a la question
de comment représenter les données textuelles en numérique. Comme la
plupart des algorithmes de machine learning sont conçus originairement
pour des données numériques ( et pas forcement pour des données
textuelles) c'est une étape très importante qui peut beaucoup changer
les résultats. Ensuite nous nous concentrons sur les différents méthodes
de création de résumé extractif (défini ci-dessus). Enfin, nous
explorons les différents méthodes d'évaluation conçues pour cette tache.
En général en machine learning, pour évaluer nos systèmes nous comparons
la sortie de nos algorithmes avec la \"vérité terrain\" (souvent
construite par des humains). Or, dans notre cas spécifique, il est
souvent très difficile d'avoir un résumé déjà fait pour pouvoir évaluer
notre système. De plus, même si on a un résumé fait par un humain, il
existe beaucoup de moyens de construire un résumé a partir d'un même
texte, donc notre résumé automatique peut être de haut qualité et au
même temps très diffèrent du résumé humain. Pour ces raisons nous
trouvons pertinent de comparer les différentes métriques pour évaluer
nos modèles.

État de l'art
-------------

Les méthodes aujourd'hui de pointe sur cette problématique se divisent
en deux catégories principales, celle par extraction de phrases et celle
abstractive:

#### Résumé extractif

La méthode par extraction consiste dans la composition du résumé en
sélectionnant les phrases les plus pertinentes du texte original. Parmi
les modèles les plus connus et qui donnent des bons résultats on trouve:

-   **BertSumExt** dont la classification est centrée sur le CLS

-   **HIBERT** qui se base sur deux transformeurs encodeurs,
    respectivement pour l'embedding de la phrase et sa probabilité
    d'être dans le resumé

-   **MatchSum** qui touche au problème à la fois au niveau des phrases
    et à la fois à celui du sommaire entier

Ce sont des modèles qui reposent sur une architecture de deep learning.
Cependant il existe plusieurs méthodes qui se reposent sur des methodes
de machine learning beaucoup plus \"classiques\" que nous explorons dans
ce projet (comme TextRank).

En dehors des méthodes spécifiquement fait pour le sommaire automatique,
nous pouvons transformer ce tache à un problème de clustering. En effet,
si nous voulons trouver un sommaire de $k$ phrases, nous pouvons
clusteriser notre texte en k clusters et prendre les k phrases qui sont
les plus proches au clusters. Donc nous pouvons utiliser tous les
méthodes standards de clustering pour obtenir un resume.

Le plus grand défaut de ce type d'approche est la manque de lisibilité.
Comme les phrases sont sélectionnées des différentes parties du texte,
nous pouvons avoir des passages très abruptes entre 2 phrases. Cela peut
être allégé (comme nous ferons dans la suite) avec des méthodes de
paraphrasage. Dans ce cadre, nous pouvons paraphraser le texte obtenu
afin d'avoir un résultat plus homogène.

Ceci sera la méthode sur laquelle nous allons nous concentrer lors des
expériences.

#### Résumé abstractif

La méthode abstractive se rapproche plus à ce qu'une personne ferait: il
cherche à comprendre le texte et produire une version condensée avec la
perte minime d'informations.

Nous touchons donc à des méthodes d'encodeurs décodeurs qui jouent avec
l'espace latent comme compréhension.

Un point très important dans ce contexte est l'attention, qui permet de
convoiter efficacement les informations importantes.

Ici nous trouvons les modèles suivants:

-   **BertSumAbs** qui produit les résumés de façon auto-regressive (un
    token à la fois)

-   **BART** (Denoising Sequence-to-Sequence Pre-training for Natural
    Language Generation, Translation, and Comprehension) qui vient pour
    reconstruire du text bruité

-   **PEGASUS** (Pre-training with Extracted Gap-sentences for
    Abstractive Summarization) dont l'entraînement est effectué par
    remplissage des masks

-   **GSUM** qui se base sur des 'guidance signals' (pouvant être
    générés par méthode extractive)

Comme les méthodes reposent sur du décodage depuis un espace latent, il
est difficile de garantir l'information factuelle de ces résumes.
Parfois, nous pouvons trouver de l'information dans le résumé qui n'est
pas contenue dans le texte original (et qui n'est donc pas correcte).
Nous appelons ce phénomène des \"hallucinations\" fait par le modèle est
ça représente l'un des plus grands défauts de ce type de méthodes.

Données
=======

#### DUC

Le dataset utilisé est celui de [DUC
2004](https://duc.nist.gov/duc2004/tasks.html), qui est conçu pour
plusieurs tâches , dont les taches 1 et 2 correspondent à des tâches de
résumé . Nous utilisons donc les données qui sont construits pour ces
deux tâches. Il est structuré par une série des documents regroupés en
clusters et de plusieurs résumés *humains* pour chaque clusters.Dans la
suite nous considérerons la concaténation de tous les documents d'un
cluster comme un corpus unique.\
![image](rapport/img/wordcloud.png)

#### News articles

Ce dataset contient des articles récupérés depuis The New York Times,
CNN, Business Insider et Breitbart. Il a été originairement produit sur
Kaggle, puis les résumés ont été ajoutés manuellement, dans le cadre de
l'article \"Evaluating Extractive Summarization Techniques on News
Articles \". Nous trouvons ici donc 1 000 lignes de données. Cette base
à été notre point de départ dans les expériences.

Méthodes
========

Nous abordons la question du résumé automatique par une approche
d'extraction de phrases. Comme tous les méthodes marchent sur des
données numériques, d'abord nous devons choisir comment représenter les
phrases par des vecteurs. Ensuite nous faisons du clustering (non
supervisé) avec le nombre de clusters équivalent à la quantité de
phrases souhaitées dans le résumé. Ensuite, pour chaque cluster nous
prenons la phrase 'la plus pertinente' et nous composons le résumé avec
ces dernières réordonnées chronologiquement. En plus des méthodes de
clustering, nous avons aussi testé le méthode TextRank qui est
spécifiquement fait pour la création des résumés automatiques par
extraction des phrases.

Représentation des phrases
--------------------------

La fondation de ce modèle se définit dans la représentation des phrases.
En effet, les résultats du clustering, qui s'appuient sur des méthodes
classiques, dépendent fortement de ce choix.

Dans le cadre des expériences nous avons débuté avec des vecteurs de
présence des mots, puis nous avons comparé avec les embeddings produits
par MPNET.

Count Vectorizer
----------------

D'abord nous pré-traitons le texte afin de ne laisser que les mots, donc
nous enlevons ponctuation, caractères spéciaux et mettons tout en
*lowercase*. Enfin nous stemmons les mots et nous obtenons ainsi nos
tokens.

Nous transformons la collection des documents en une matrice de compte
des tokens (i.e. les mots). Cela revient à une logique du 'si les
phrases ont les mêmes mots (en particulier les mêmes entités nommées)
alors elles nous apportent les mêmes informations\".

Vu que nous définissons le vocabulaire ad hoc sur les documents, nous
enlevons les *stopwords* et nous ne coupons pas sur les fréquences
minimales et maximales des mots.

Pour ce qui concerne les entités nommées, elles appartiennent au
vocabulaire car les vecteurs sont appris sur toute la collection des
documents qu'on résume. Donc elles pondèrent dans le rapprochement des
phrases. Si dans d'autres tâches cela peut ramener à du
sur-apprentissage, dans ce cas c'est un très bon facteur.

![image](rapport/img/tsne_count.png)

Afin de s'orienter dans les données, nous avons effectué du t-sne sur
les vecteurs obtenus. La couleur, correspond à l'ordre chronologique
d'une phrase dans le texte. Nous trouvons ici des données très
éparpillées, difficiles à interpréter à l'œil.

Embeddings
----------

Afin d'obtenir les embeddings des phrases, nous utilisons le modèle
[all-mpnet-base-v2](https://huggingface.co/sentence-transformers/all-mpnet-base-v2)
qui est un *sentence transformer*. Nous avons choisi ce modèle car il
performe très bien sur des taches de similarité des phrases, ce qui veut
dire que des phrases semantiquement proches sont aussi proches dans
l'espace latente.

Grâce à ceci nous pouvons encoder les phrases, ce qui revient à la
projection dans l'espace latent.

Le calcul des embeddings par cette méthode est très coûteux en terme de
calcul, mais en travaillant toujours sur le même dataset nous avons
calculé une seule fois les représentations. Cela nous a permis de faire
une multitude d'expérimentations rapidement, mais c'est un coût à
prendre en compte lors de l'application sur des nouveaux textes.

![image](rapport/img/tsne_embed.png)

En opposition à l'exemple antécédent nous observons ici que les phrases
sont regroupées par ordre chronologiques, i.e. on retrouve les phrases
dans la même portion du texte assez proche. Ce résultat nous parait
cohérent et nous donne espoir de clusteriser 'mieux' les phrases, donc
d'extraire les plus différentes.

Clustering
----------

Nous utilisons du clustering non supervisé afin de diviser les phrases
par 'topic'. En suivant cette stratégie, en piochant un élément par
cluster, nous devrions récupérer les phrases plus diverses en termes de
contenu.

Il faut remarquer que notre modèle se base sur ce principe et il n'y a
aucune analyse directe au niveau des entités nommées. Cela permettrait
d'avoir des meilleurs résultats mais ce ne sera pas dans le cadre de ce
projet.

Dans la pratique, nous apprenons le modèle sur les vecteurs de l'espace
latent. Ensuite nous calculons les centroides/componentes d'un modèle et
nous récupérons la phrase la plus proche à chacun de ceux-ci selon la
distance utilisé lors de l'apprentissage.

K-means
-------

Voici a méthode standard de clustering. Nous l'appliquons donc sur les
vecteurs obtenus lors de la projection.

SVD
---

Une autre méthode qui s'applique bien avec les matrices que nous
obtenons depuis les textes. Tout comme k-means elle s'applique
directement.

LDA
---

Cette méthode se distingue des autres car elle s'appuie sur des matrices
positives, ce qui est le cas pour la vectorisation par comptage mais pas
pour les embeddings. Donc si on peut avoir des très bons résultats avec
LDA (ce sera approfondi dans la section des résultats), il est par
construction pas utilisable avec les embeddings. Il serait possible de
*tweak* afin de le faire marcher, mais cela ne produit pas bien
évidemment des bons résultats.

TextRank 
--------

Nous avons implémenté TextRank à la main en suivant la description de
l'algorithme dans l'article original. Cet algorithme est très proche a
celui de PageRank.

#### Stopwords

Dans la représentation des mots par sac de mots pour tous les autres
méthodes nous avons toujours enlevé les stopwords. Or, dans l'article
original de TextRank nous voyons qu'ils obtiennent des meilleurs
résultats en gardant les stopwords dans le texte donc nous avons décidé
de tester ce méthode avec et sans la suppression des stopwords.

![image](rapport/img/textrank_scores_count.png)

Finalement, sur nos données, nous observons que TextRank avec stopwords
performe mieux que au sens du recall\@1.

Paraphrase
----------

Pour ce qui concerne la paraphrase, le but principale est de transformer
des phrases détachées et sans lien en un texte plus 'homogène'. Nous
essayons donc d'augmenter la lisibilité (dont la signification et
évaluation sera abordé plus tard) tout en gardant les informations,
aussi en portant une attention particulière au phénomène des
informations fantasme typiques de cette méthode.

Les modèles qui accomplissent cette tâche sont très lourds à entraîner,
donc nous avons choisi de s'appuyer sur la ressource [hugging
face](https://huggingface.co/models) afin d'en récupérer certains
pre-entraînés. Par conséquent, nous avons peu de marge de modification
sur cette phase. Nous y dédierons une mineure attention dans la suite.

### Pegasus

Ce modèle est notamment entraîné sur la tache de synthétisation
automatique des textes. Pour cette raison nous remarquons que les textes
produits par PEGASUS sont souvent plus courts que les originales.
Cependant, il reste l'un des modèles les plus utilisées pour le
paraphrasage des textes, donc nous l'avons inclus.

### T5

Ce modèle est entraîné sur plusieurs taches, dont le paraphrasage de
textes. Nous trouvons ici une structure qui rappelle les
encodeurs-decodeurs, avec une forte présence du concept d'attention.

Nous remarquons que sa paraphrase tend a couper certain mots plutôt que
les substituer, ce qui nous fait revenir à la tache de synthèse tout
comme Pegasus.

    Original Girding for four years of potential battles with Donald J Trump, Democratic leaders of the California Legislature announced Wednesday that they had hired Eric H Holder Jr who was attorney general under President Obama, to represent them in any legal fights against the new Republican White House. He said California Democrats decided to turn to Mr Holder as they watched Mr Trump assemble his cabinet and begin to set the tone for his presidency.
  ---------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     Pegasus The Democratic leaders of the California Legislature hired Eric H Holder Jr. , who was attorney general under President Obama, to represent them in any legal fights against the new Republican White House. He said California Democrats decided to turn to Mr Holder as they watched Mr Trump assemble his cabinet and begin to set the tone for his presidency.
          T5 After four years of potential fights with Donald J Trump, Democratic leaders of the California Legislature announced Wednesday that they had hired Eric H Holder Jr, who was Attorney General under President Obama, to represent them in any legal fights against the new Republican White House. He said California Democrats had decided to turn to Mr Holder as they watched Mr Trump as he assembled his cabinet and began to set the tone for his presidency.

Évaluation
==========

Comme évoqué dans l'introduction, l'évaluation d'un résumé est toujours
une question ouverte dans le monde de la recherche. En effet, il y a
plusieurs façons de créer un bon résumé donc il est difficile de
quantifier à quel point un résumé est bien fait. Il est ni judicieux ni
faisable d'évaluer les résumés à la main. Une métrique est nécessaire.
Le cœur de ce projet se place donc dans l'évaluation d'un résumé.

Qu'est ce que définit un bon resumé? Nous identifions quelques
'principes':

-   Il contient un maximum d'informations de la source.

-   Il ne rajoute pas de l'information qui n'existe pas dans la source.

-   Il est lisible.

-   Il est sensiblement plus court que la source.

Or, la plupart des méthodes existants ne cherchent pas à définir ce qui
fait un bon résumé, mais plutôt évaluent à quel point le résumé généré
est similaire à un résumé écrit par un humain (qu'on estime comme de
facto le résumé parfait).

Nous cherchons donc les mêmes entités nommées dans la source et le
résumé. Cela peut être fait de façon 'implicite' avec du tf-idf ou par
simple correspondance comme tout autre mot, mais aussi de façon
'explicite' en vérifiant la présence dans les deux textes des mêmes
entités. Ceci est le cas par exemple de QuestEval, ceci sera approfondi
plus tard.

Nous commençons par les scores classiques ROUGE et BLEU. Ils sont
considérés un must en considérant leur corrélation à l'avis humain par
rapport à leur simplicité. Il se basent tous les deux sur la comparaison
du résumé généré avec un résumé écrit par un humain qu'on estime comme
la \"vérité terrain\". Notamment ils regardent à quel point les deux
résumés utilisent les mêmes mots. Ensuite nous passons sur BERTscore qui
aussi compare la similarité entre le résumé généré et un résumé écrit
par un humain, mais cette fois-ci dans l'espace latent crée par BERT.
L'espoir dans cet approche est qu'une similarité dans cet espace puisse
interpréter mieux la similarité sémantique, même si on n'utilise pas les
mêmes mots. Enfin nous passons sur QuestEval qui n'a pas besoin de
\"vérité terrain\" pour bien fonctionner. Sur toutes ces méthodes nous
allons nous intéresser à la prise en compte de la complétude et de la
précision du résume.

Rouge
-----

Rouge (Recall-Oriented Understudy for gisting evaluation) est une
métrique conçue pour la tache de résumé.[@lin_rouge_2004] Aujourd'hui
elle est la métrique standard du domaine. Globalement, elle considère la
précision et le rappel entre le résumé et les références, i.e. des
résumés produit par des personnes.

Nous allons retrouver *rouge-1* qui compare les uni-grammes, *rouge-2*
pour les bi-grammes et enfin *rouge-l* pour la plus longue séquence en
commun (pas forcément consécutive).

Cette métrique donne des bons résultats de corrélation humaine malgré sa
simplicité. On se doute qu'il serait possible de trouver mieux mais
développer une métrique qui soit fiable est tout un art.

Bleu
----

Associé à Rouge, nous trouvons Bleu, qui est originalement conçue pour
la tâche de traduction machine, mais est toujours utilisé dans
l'évaluation de résumés automatiques. [@papineni_bleu_2002]

Encore une fois nous avons une comparaison entre le texte candidat
(produit par la machine) et les références humaines.

Celle ci est plus concentrée sur la couverture.

Pour cette raison nous allons la prendre en compte mais sans s'y
concentrer trop lors de l'analyse des résultats.

BERTscore
---------

BERTScore est une métrique d'évaluation originalement conçue pour la
similarité entre deux phrases. Comme ROUGE et BLEU, nous pouvons aussi
utiliser BERTScore pour mesurer le similarité entre le résumé généré et
un résumé \"vérité terrain\".[@zhang_bertscore_2020]

D'abord nous remarquons que les scores semblent être élevés pour
n'importe quelles deux phrases. Cependant, même si tous les scores sont
grands nous voyons que des couples de phrases ont des scores plus hautes
que d'autres couples de phrases non-similaires. Donc ça reste une
métrique intéressante.

Il est important de noter que BERTScore est originalement fait pour
comparer 2 phrases. Dans notre cas, nous comparons 2 textes avec le même
nombre de phrases, mais il y a une ambiguïté de comment choisir la
correcte permutation de phrases (autrement dit, choisir quelle phrase
dans le résumé humain correspond à quelle phrase dans le résumé
automatique . Comme nous voyons, le matching des phrases peut avoir un
fort impact sur le score obtenu. Lors de nos résultats, nous avons
décidé de prendre le matching qui maximise la somme de similarités
cosine entre les phrases (où les embeddings des phrases sont calculé
comme décrit dans la partie 3.iii). Nous avons choisi cela pour ne pas
pénaliser un résumé si il présente exactement les mêmes informations
dans un ordre diffèrent.

QuestEval
---------

La différence la plus importante entre ce méthode d'évaluation et tous
les autres est le fait qu'on n'a pas besoin d'une vérité terrain pour
évaluer un résumé en utilisant ce méthode. L'intuition derrière ce
méthode est qu'on estime qu'un résumé est bon si il comporte les mêmes
informations comme le texte source.[@scialom_questeval_2021]

Le système commence par trouver tous les entités nommées du texte
source, et ensuite génère une question pour chaque entité nommée (dont
le réponse est cette entité ). Enfin le système essaye de voir si le
système de question answering peut répondre a ces questions avec les
informations du résumé.

Comme c'est un modèle de deep learning qui est utilisé pour évaluer les
modèles, c'est encore plus lourd que BERTScore ( qui est déjà beaucoup
plus lourd que ROUGE et BLEU).

À cause de cela, nous n'avons pas réussi à le tester proprement
nous-mêmes.

Résultats
=========

Embeddings
----------

### DUC

Dans ce contexte nous remarquons que kmeans obtient le
meilleur score selon presque toutes les métriques. Cependant nous voyons
que TextRank performe mieux sur le f1 score et la precision produit par
BERTScore.

![image](rapport/img/scores_embed.png)

En effet, textrank est conçu sur les *bag of words*, donc ce n'est pas
étonnant que es méthode 'souffre' dans le contexte des embeddings. De
plus, comme la distance dans l'espace latent est directement liée à la
distance sémantique, donc des méthodes qui raisonnent en termes des
'distance' sont censées marcher le mieux. C'est bien ce qu'on retrouve
ici.

En observant les longueurs moyennes des différentes approches, nous ne
remarquons pas une différence substantielle, mais nous surlignons la
variance plus élevée de kmeans. ![image](rapport/img/length_embed.png) Nous
n'en tirons donc pas des considérations sur l'une ou l'autre méthode.

### News articles

Pour ce qui concerne les données du dataset news, nous trouvons encore
kmeans comme la méthode plus performante selon tous les scores.

![image](rapport/img/scores_embed_excel.png)

En observant les longueur moyennes de textes, nous ne trouvons toujours
pas des différences remarquables.
![image](rapport/img/length_embed_excel.png)

Enfin, nous constatons que les considérations de la section antécédentes
s'appliquent et se confirment aussi avec ce dataset.

Count
-----

### DUC

Nous observons qu'il n'y a aucun modèle qui se démarque
spécialement. Cependant sur le recall lda est celui qui marche le mieux,
et pour la précision on trouve textrank.

![image](rapport/img/scores_count.png)

En faisant encore une observation des longueurs moyennes des résumés,
nous remarquons que lda produit ceux plus longs, nettement.
![image](rapport/img/length_count.png) Ceci explique ses bons scores en
rappel. Si nous plongeons dans l'analyse, lda a aussi la pire précision.
Avoir le plus d'information au prix de pas avoir les plus intéressantes
nous fait douter de cette méthode.

### News articles

Pour ce qui concerne les données news on obtient des résultat assez
différents (voir fig. [2](#fig:excel_count){reference-type="ref"
reference="fig:excel_count"}). Textrank est le modèle qui sort
clairement des meilleurs résultats. Nous pouvons attribuer cette
différence à la différence structure entre les deux bases de données. En
effet, comme dans les données DUC on analyse une concaténation d'une
dizaine de documents (qui parlent tous d'un même sujet) il est possible
que textRank s'en sorte pire dans un tel cadre. Par contre dans le cadre
d'un texte classique, il s'en sort beaucoup mieux. Celui ci est suivi
par lda, qui se distingue de kmeans et svd, mais qui n'arrive pas à être
concurrent à textRank.

![image](rapport/img/scores_count_excel.png)

Par contre, en observant les longueurs moyennes des textes, nous
trouvons que kmeans et svd produisent des résultats substantiellement
plus courts que lda et textrank.
![image](rapport/img/length_count_excel.png) En suivant le raisonnement
appliqué aux résultats qui concernent duc, nous aurons envie de remettre
en question textrank. Or nous observons aussi une précision plus élevée,
donc nous surlignons la puissance de textrank dans ce cadre.

Comparaison
-----------

Entre les deux types de représentations nous ne sommes pas capables de
déterminer un 'gagnant' avec les métriques à notre disposition. En
effet, les résultat sont extrêmement proches. Cela est intéressant, car
nous voulions diminuer le fossé sémantique en passant par MPNET,
cependant ce n'est pas suffisant pour extraire des phrases plus
pertinentes.

Nous nous demandons si une approche sémantique serait plus efficace si
prise entièrement dans cette optique, alors que nous prenons cela pour
la représentation mais terminons par une approche extractive plus
statistique et de 'bas niveau'.

Paraphrase
----------

Nous avons aussi testé les performances de nos modèles de paraphrasage.
Nous avons décidé de prendre un seul modèle par base de documents, car
nous avons jugé que la performance des modeles de paraphrasage ne doit
pas dépendre ni des modèles utilisés pour obtenir le résumé qu'on veut
paraphraser, ni de la représentation des données qu'on avait choisi
avant d'appliquer ce modèle. De plus, il est important de noter que l'un
des gros avantages du methodes de paraphrasage est l'augmentation de
visibilité, or , nos métriques ne capturent pas du tout cet aspect.

### DUC

Sur les données DUC nous voyons qu' il y a les résumés paraphrasés par
pegasus qui dominent en termes de précision. Il est intéressant de noter
que les résumés écrits par pegasus ont des pires scores en rappel que
les 2 autres modèles pour toutes les versions de ROUGE, mais il est
aussi bien que les autres en termes de rappel de BERTScore. Nous pouvons
attribuer cela au fait que rouge pénalise le modèle s'il n'utilise pas
exactement les mêmes mots que le résumé \"vérité terrain", tandis que
BERT Score nous permet d'utiliser des synonymes sans être pénalisé .
Nous voyons cependant que pegasus reste en premier en termes de f1 score
sur toutes les métriques.

![image](rapport/img/scores_paraphrase_embed.png)

Comme mentionné précédemment, pegasus est entraîné de base sur la tâche
de création de résumé automatique. Donc même cette version fine-tuned
pour la tâche de paraphrasage réduit la longueur des résumés en moyenne.
Nous notons la corrélation entre la réduction de la longueur moyenne
avec l'augmentation de la performance en termes de précision.

![image](rapport/img/length_paraphrase_embed.png)

### News Articles

Sur les données News Articles par contre, nous voyons que le résumé
original domine sur toutes les métriques, sauf la précision de
BERTScore. Comme noté précédemment, cela ne veut pas dire que sur ces
données la paraphrasage est inutile, vu que l'un de ces plus grands
avantages (la lisibilité) n'est pas du tout prise en compte par ces
métriques.

![image](rapport/img/scores_paraphrase_embed_excel.png)

Comme avant nous voyons que le résumé paraphrasé par pegasus est
significativement plus court que les autres.

![image](rapport/img/length_paraphrase_embed_excel.png)

Ouverture
=========

Nous allons aborder dans cette section les sujets que nous avons abordé
ou auquel nous avons porté intérêt mais que nous n'avons pas choisi
d'explorer en profondeur dans le cadre de ce projet.

#### Contexte d'utilisation

Si l'idée de résumé/synthèse est très générale, nous découvrons que les
méthodes sont souvent plus spécifiques. Notamment pour les résumés
extractifs, ils peuvent être très efficaces pour les articles de
journaux ou news, du moment qu'on va essayer d'extraire des 'facts' qui
se retrouvent dans des phrases individuelles.

Ce n'est plus du tout le cas pour la narration par exemple, où il est
évident que en extrapolant des phrases, il sera impossible parcourir le
récit. Ceci est un domaine encore ouvert, qui pose un grand défi.
Cependant nous affirmons qu'il est sans doutes mieux passer par des
méthodes abstractives. Dans ce cas, il faut bien 'comprendre'
sémantiquement le texte afin de pouvoir en produire un bon résumé.

#### Manque d'entités nommées

Lors de ce projet, nous avons traité directement les phrases et leur
embeddings. Cependant nous sommes persuadés que l'utilisation des
entités nommées pourrait sensiblement améliorer les performances des
méthodes. Cela compliquerait la pipeline de traitement, donc nous ne
l'avons pas implémenté. Nous soulignons cette possibilité avec
l'introduction de models tels que les LSTM, ce qui serait implémentable
à la fois dans les méthodes extractives et abstractives.

Si cela serait intéressant à étudier lors de la tâche de synthèse, nous
avons déjà rencontré ce concept avec QuestEval, qui l'utilise lors de
l'évaluation.

#### Lisibilité

Les métriques sont fondamentales pour améliorer un modèle. Nous avons vu
qu'elles sont relativement pauvres dans le contexte du resumé
automatique.

Notamment dans la tache de paraphrasage, nous cherchons pas à améliorer
le résumé dans contenu mais plutôt dans la forme et la lisibilité. Or
nous n'avons pas de métrique qui permet d'analyser directement une
amélioration de la lisibilité. Toutefois nous avons pu chercher à
identifier des hallucinations avec rouge par exemple.

Nous souhaitons donc explorer ce sujet ultérieurement.

Conclusion
==========

Dans l'adventure de ce projet, nous avons pu étudier et implémenter
plusieurs méthodes de résumé automatique. Nous avons donc pris
conscience des différentes approches, de leur placement chronologique,
des avancées et de la tendance actuelle. Nous observons comment les
modèles de deep se sont instaurés, à la fois en donnant place à des
modèles abstractifs et aussi en mettant à jour ceux classiques
extractifs.

En suite, nous avons porté notre attention sur la difficulté à évaluer
les résumés. Cela se répercute sur un 'bottleneck' provoqué par les
métriques, pas assez représentatives néanmoins pas moins importantes.
Elles sont fondamentales pour l'apprentissage d'un modèle et pour notre
potentiel et capacité à améliorer ces derniers.

Nous considérons donc que la clé pour avancer dans le domaine se pose
dans les métriques. Aujourd'hui, nous avançons à tâtons et un peu à
l'aveugle, en évaluant avec l'experience métier, qui se révèle efficace
mais pas suffisante. Quand elles seront plus représentatives et donc
puissantes dans la pratique, nous supposons que les modèles performantes
viendront à la suite.
