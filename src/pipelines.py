import nltk.tokenize

from src import clustering
from src.preprocessing import *

"""
PREPROCESS
"""


def fast_pipeline_corpus(filename):
    txt = parse_file(filename)
    prep = preprocess_corpus(txt)
    stem = stem_corpus(prep)
    v, X = vectorize_vocabulary(stem)
    cleaned = preprocess_corpus(txt, ponc=True, specials=True, lower=False, numbers=False)
    return v, X, cleaned


def fast_pipeline_docs(path):
    txt = parse_folder(path)
    prep = preprocess_docs(txt)
    stem = stem_docs(prep)
    v, X = vectorize_vocabulary_multidoc(stem, min_df=0.01, max_df=0.9)

    cleaned = preprocess_docs(txt, ponc=True, specials=True, lower=False, numbers=False)
    return v, X, cleaned


def preprocess_duc(corpuses: list, remove_stopwords=True, min_df=0., max_df=1.) -> list:
    """

    :param corpuses: list of list of sentences
    :param remove_stopwords: bool
    :return:list of numpy matrixes where each matrix represents a document
        each numpy matrix contains as many rows as there are sentences in the document
        (each row represents a sentence)
    """
    prep = preprocess_docs(corpuses, ponc=True, specials=True, lower=True, numbers=True)
    stem = stem_docs(prep, remove_stopwords=remove_stopwords)
    return [vectorize_vocabulary(text, min_df=min_df, max_df=max_df)[1] for text in stem]


"""
Clustering
"""


def all_summaries(models_summaries, nb_sentences, human_summaries, xes_norm, parsed_content, norm=None):
    if type(nb_sentences) is int:
        nb_sentences = [nb_sentences] * len(human_summaries)

    for i in range(len(xes_norm)):
        number_sentences = nb_sentences[i]
        km = sklearn.cluster.KMeans(number_sentences)
        svd = sklearn.decomposition.TruncatedSVD(n_components=number_sentences, n_iter=20)
        lda = sklearn.decomposition.LatentDirichletAllocation(n_components=number_sentences, max_iter=20)

        km.fit(xes_norm[i])
        svd.fit(xes_norm[i])
        lda.fit(xes_norm[i])

        kmeans_indexes = np.sort(np.array(clustering.doc_clustering(km.cluster_centers_, xes_norm[i], norm=norm)))
        svd_indexes = np.sort(np.array(clustering.doc_clustering(svd.components_, xes_norm[i], norm=norm)))
        lda_indexes = np.sort(np.array(clustering.doc_clustering(lda.components_, xes_norm[i], norm=norm)))

        models_summaries.setdefault('kmeans', []).append(" ".join(np.array(parsed_content[i])[kmeans_indexes]))
        models_summaries.setdefault('svd', []).append(" ".join(np.array(parsed_content[i])[svd_indexes]))
        models_summaries.setdefault('lda', []).append(" ".join(np.array(parsed_content[i])[lda_indexes]))


def kmeans_summaries(nb_sentences, matrixes, parsed_content, norm=None,return_outliers=False):
    res = []
    outliers=[]

    for i in range(len(matrixes)):
        number_sentences = nb_sentences[i]
        km = sklearn.cluster.KMeans(number_sentences)
        km.fit(matrixes[i])
        indices = np.sort(np.array(clustering.doc_clustering(km.cluster_centers_, matrixes[i], norm=norm)))
        reparsed = [str(a.replace('.', '')) + '.' for a in parsed_content[i]]  # JE MODIFIE ICI
        res.append(_clean_join(np.array(reparsed)[indices]))

        nb_clusters = len(km.cluster_centers_)
        nb_indices = len(indices)
        tokens = len(nltk.tokenize.sent_tokenize(res[i]))
        if not tokens == number_sentences:
            outliers.append(i)
            """print(f'nb_sentences {number_sentences} demandes')
            print(f'tokenized {tokens} obtenus')
            print(f'nb_clusters {nb_clusters}')
            print(f'nb_phrases ' + str(len(np.array(parsed_content[i])[indices])))
            # print(np.array(parsed_content[i])[indices])
            raise Exception('longeuur' + str(i))"""
    
    if return_outliers:
        return res,outliers
    else:
        return res


def svd_summaries(nb_sentences, matrixes, parsed_content, norm=None,return_outliers=False):
    res = []
    outliers=[]
    
    for i in range(len(matrixes)):
        number_sentences = nb_sentences[i]
        svd = sklearn.decomposition.TruncatedSVD(n_components=number_sentences, n_iter=20)
        svd.fit(matrixes[i])
        indices = np.sort(np.array(clustering.doc_clustering(svd.components_, matrixes[i], norm=norm)))
        res.append(_clean_join(np.array(parsed_content[i])[indices]))
        
        tokens = len(nltk.tokenize.sent_tokenize(res[i]))
        if not tokens == number_sentences:
            outliers.append(i)

    if return_outliers:
        return res,outliers
    else:
        return res


def lda_summaries(nb_sentences, matrixes, parsed_content, norm=None,return_outliers=False):
    res = []
    outliers=[]
    for i in range(len(matrixes)):
        number_sentences = nb_sentences[i]
        lda = sklearn.decomposition.LatentDirichletAllocation(n_components=number_sentences, max_iter=20)
        lda.fit(matrixes[i])
        indices = np.sort(np.array(clustering.doc_clustering(lda.components_, matrixes[i], norm=norm)))
        res.append(_clean_join(np.array(parsed_content[i])[indices]))
        
        tokens = len(nltk.tokenize.sent_tokenize(res[i]))
        if not tokens == number_sentences:
            outliers.append(i)

    if return_outliers:
        return res,outliers
    else:
        return res


def textrank_summaries(nb_sentences, matrixes, parsed_content,return_outliers=False):
    indices = [clustering.textRank(matrixes[i], nb_clusters=nb_sentences[i]) for i in range(len(matrixes))]
    summaries = [np.array(parsed_content[i])[indices[i]] for i in range(len(parsed_content))]
    res=list(map(_clean_join, summaries))
    
    outliers=[]
    for i in range(len(res)):
        tokens = len(nltk.tokenize.sent_tokenize(res[i]))
        if not tokens == nb_sentences[i]:
            outliers.append(i)

    if return_outliers:
        return res,outliers
    else:
        return res


def _clean_join(texts: str):
    texts = [str(a.replace('.', '')) + '.' for a in texts]
    return ' '.join(texts)
