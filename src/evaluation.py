import sys

import rouge

sys.path.append('..')
from src.preprocessing import *
import bert_score
import torch
from sentence_transformers import SentenceTransformer
from scipy.optimize import linear_sum_assignment
from typing import List
# nltk.translate.bleu_score.sentence_bleu
import pandas as pd
import numpy as np
import tqdm


def sentence_rouge(refs: list, hyps: list, avg: bool = False):
    # returns ROUGE scores
    r = rouge.Rouge()
    refs = np.array(refs)
    hyps = np.array(hyps)
    scores = r.get_scores(hyps, refs, avg=avg)
    a = pd.json_normalize(scores, sep='_').to_dict(orient='records')
    return a[0]


def sentence_bleu(refs: list, hyps: list):
    tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')

    refs_token = [[tokenizer.tokenize(summary) for summary in text] for text in refs]
    hyps_token = [tokenizer.tokenize(a) for a in hyps]
    return nltk.translate.bleu_score.corpus_bleu(refs_token, hyps_token)


def list_summaries_bert_score_fast(list_human_summaries: list, list_generated_summaries: list, pairing_function=None):
    assert len(list_human_summaries) == len(list_generated_summaries), 'len human and generated summaries'
    list_human_summaries = parse_list_of_texts(list_human_summaries)
    list_generated_summaries = parse_list_of_texts(list_generated_summaries)
    human_sentences = []
    generated_sentences = []

    lengths = [0]
    if pairing_function is not None:
        model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2', cache_folder='/tmp/trans')
    for i in tqdm.tqdm(range(len(list_human_summaries))):
        human_sentences += list_human_summaries[i]
        assert len(list_human_summaries[i]) == len(
            list_generated_summaries[i]), 'phrases human and generated summary not correspondant -> iter ' + str(i)
        lengths.append(lengths[-1] + len(list_human_summaries[i]))
        if pairing_function is not None:
            generated_sentences += pairing_function(list_human_summaries[i], list_generated_summaries[i], model)
        else:
            generated_sentences += list_generated_summaries[i]

    P, R, F1 = bert_score.score(generated_sentences, human_sentences, lang="en", verbose=False)
    Prec = np.mean(
        np.array([torch.mean(P[lengths[i]:lengths[i + 1]]).item() for i in range(len(list_human_summaries))]))
    Recall = np.mean(
        np.array([torch.mean(R[lengths[i]:lengths[i + 1]]).item() for i in range(len(list_human_summaries))]))
    F1Score = np.mean(
        np.array([torch.mean(F1[lengths[i]:lengths[i + 1]]).item() for i in range(len(list_human_summaries))]))

    return Prec, Recall, F1Score


def list_summaries_bert_score(list_human_summaries: list, list_generated_summaries: list, pairing_function=None):
    assert len(list_human_summaries) == len(
        list_generated_summaries), 'longueur de list summaries humains et generé non correspondant'
    Pmean, Rmean, F1mean = (0.0, 0.0, 0.0)
    for i in tqdm.tqdm(range(len(list_human_summaries))):
        P, R, F1 = summary_bert_score(list_human_summaries[i], list_generated_summaries[i], pairing_function)
        Pmean += P
        Rmean += R
        F1mean += F1
    Pmean = Pmean / len(list_human_summaries)
    Rmean = Rmean / len(list_human_summaries)
    F1mean = F1mean / len(list_human_summaries)
    return Pmean, Rmean, F1mean


def summary_bert_score(human_summary: str, generated_summary: str, pairing_function=None):
    list_generated_summary = parse_text(generated_summary)
    list_human_summary = parse_text(human_summary)
    if pairing_function is not None:
        list_generated_summary = pairing_function(list_human_summary, list_generated_summary)
    P, R, F1 = bert_score.score(list_generated_summary, list_human_summary, lang="en", verbose=False)
    return torch.mean(P).item(), torch.mean(R).item(), torch.mean(F1).item()


def optimal_matching(list_human_sentences: List[str], list_generated_sentences: List[str], model=None):
    assert len(list_human_sentences) == len(list_generated_sentences)
    if model is None:
        model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2', cache_folder='/tmp/trans')
    human_embeddings = model.encode(list_human_sentences, normalize_embeddings=True)
    generated_embeddings = model.encode(list_generated_sentences, normalize_embeddings=True)
    simmilarity_matrix = human_embeddings @ generated_embeddings.T

    # optimal_pairings is a tuple of 2 arrays arr1 and arr2 such that
    # for all i arr1[i] is the index of the sentence in human_embeddings closest to the sentence in generated_embeddings with index arr2[i]
    # linear_sum_assignment(arg) gives the optimal_pairings that minimises arg (where arg is a non-negative matrix)
    # minimize 2-simmilarity_matrix <=> maximize simmilarity_matrix
    optimal_pairings = linear_sum_assignment(2 - simmilarity_matrix)

    # initialisation
    paired_list_generated_sentences = ['a'] * len(list_generated_sentences)

    for i in range(len(list_generated_sentences)):
        paired_list_generated_sentences[optimal_pairings[0][i]] = list_generated_sentences[optimal_pairings[1][i]]

    return paired_list_generated_sentences


def least_optimal_matching(list_human_sentences: List[str], list_generated_sentences: List[str], model=None):
    assert len(list_human_sentences) == len(list_generated_sentences)
    if model is None:
        model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2', cache_folder='/tmp/trans')
    human_embeddings = model.encode(list_human_sentences, normalize_embeddings=True)
    generated_embeddings = model.encode(list_generated_sentences, normalize_embeddings=True)
    simmilarity_matrix = human_embeddings @ generated_embeddings.T

    # optimal_pairings is a tuple of 2 arrays arr1 and arr2 such that
    # for all i arr1[i] is the index of the sentence in human_embeddings closest to the sentence in generated_embeddings with index arr2[i]
    # linear_sum_assignment(arg) gives the optimal_pairings that minimises arg (where arg is a non-negative matrix)
    # minimize 2-simmilarity_matrix <=> maximize simmilarity_matrix
    optimal_pairings = linear_sum_assignment(simmilarity_matrix)

    # initialisation
    paired_list_generated_sentences = ['a'] * len(list_generated_sentences)

    for i in range(len(list_generated_sentences)):
        paired_list_generated_sentences[optimal_pairings[0][i]] = list_generated_sentences[optimal_pairings[1][i]]

    return paired_list_generated_sentences


def random_matching(list_human_sentences: List[str], list_generated_sentences: List[str], model=None):
    assert len(list_human_sentences) == len(list_generated_sentences)

    p = np.random.permutation(len(list_human_sentences))

    return list(np.array(list_generated_sentences)[p])
