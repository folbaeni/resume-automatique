from itertools import chain

import matplotlib.pyplot as plt
import numpy as np
from wordcloud import WordCloud, STOPWORDS

stopwords = set(STOPWORDS)


def show_wordcloud(data, title=None):
    wordcloud = WordCloud(
        background_color='white',
        stopwords=stopwords,
        max_words=200,
        max_font_size=40,
        scale=3,
        random_state=1 # chosen at random by flipping a coin; it was heads
    ).generate(str(data))

    fig = plt.figure(1, figsize=(12, 12))
    plt.axis('off')
    if title:
        fig.suptitle(title, fontsize=20)
        fig.subplots_adjust(top=2.3)

    plt.imshow(wordcloud)
    plt.show()


def show_distances(centres, vectors):
    """ liste des centres, listes des phrases vectorisees -> img"""
    distances = np.zeros_like(centres@vectors.T)
    #print(distances.shape)
    for i in range(len(centres)):
        for j in range(len(vectors)):
            distances[i,j] = np.sum(np.abs(centres[i] -  vectors[j]))

    plt.imshow(distances, cmap='Reds')
    plt.colorbar()
    plt.show()


def avg_words_per_summary(list_of_summaries):
    words_per_summary = 0
    for i in range(len(list_of_summaries)):
        words_per_summary += len(list_of_summaries[i].split())
    return words_per_summary / len(list_of_summaries)


def recompose_summaries(corpus: list) -> list:
    """

    :param corpus: list of summaries, summary is list of sentences
    :return:
    """
    return [' '.join(a) for a in corpus]


class Duc(object):
    def __init__(self, clusters_ids, docs_ids, cluster_summaries, data):
        self.clusters_ids = clusters_ids
        # clusters_ids.sort() on fait deja le sort dans dataset manipulation
        self.docs_ids = docs_ids
        self.cluster_summaries = cluster_summaries
        self.text = data

    def corpus_cluster(self, cluster_id):
        """

        :param cluster_id: int
        :return: list of list of sentences
        """
        return list(self.text[self.text['cluster'] == cluster_id]['text'])

    def docs_ids_list(self):
        return np.array([a for a in self.docs_ids.values()]).flatten()

    def clusters_corpuses(self):
        return [list(chain(*self.text[self.text['cluster'] == c]['corpus'])) for c in self.clusters_ids]

    def summaries_corpuses(self):
        return [self.cluster_summaries[c] for c in self.clusters_ids]

    def summaries_texts(self):
        return [[' '.join(a) for a in self.cluster_summaries[c]] for c in self.clusters_ids]
