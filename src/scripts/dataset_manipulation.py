import os
import pickle
import xml.etree.cElementTree as et

import nltk
import pandas as pd


def parse_text(text: str) -> str:
    try:
        res = nltk.tokenize.sent_tokenize(text)
    except Exception as e:
        print(e)
        nltk.download('punkt')
        res = nltk.tokenize.sent_tokenize(text)
    return res


task_12_path = '../../data/DUC2004_Summarization_Documents/duc2004_testdata/tasks1and2/duc2004_tasks1and2_docs/docs/'


# %%


clusters_ids = os.listdir(task_12_path)
docs_ids = dict()
for cluster in clusters_ids:
    docs_ids[cluster] = os.listdir(task_12_path + cluster)

# %%


tree = et.parse(task_12_path + clusters_ids[0] + "/" + docs_ids[clusters_ids[0]][0])
root = tree.getroot()

# %%

df = pd.DataFrame(columns=['cluster', 'document', 'corpus'])
for cluster in clusters_ids:
    for doc in docs_ids[cluster]:
        try:
            tree = et.parse(task_12_path + cluster + "/" + doc)
            root = tree.getroot()
            for t in root.iter('TEXT'):
                tt = t.text.replace('\n', '')

                df.loc[len(df)] = [cluster, doc, parse_text(tt)]
        except:
            print('a')

df.sort_values(by=['cluster', 'document'], ascending=[True, True], ignore_index=True, inplace=True)
print(df)

# %%
models_path = '../../data/duc2004_results/ROUGE/duc2004.task2.ROUGE.models/eval/models/2/'
models_ids = os.listdir(models_path)

cluster_refs = dict()
for cluster in clusters_ids:
    try:
        cluster_refs[cluster] += [x for x in models_ids if cluster[1:-1] in x]

    except KeyError:
        cluster_refs[cluster] = [x for x in models_ids if cluster[1:-1] in x]

# %%

cluster_summaries = dict()
for cluster in clusters_ids:
    summaries = []
    for ref in cluster_refs[cluster]:
        file = open(models_path + ref, mode='r')
        all_txt = file.read()
        file.close()
        summaries.append(parse_text(all_txt.replace('\n', ' ')))
    cluster_summaries[cluster] = summaries

# print(cluster_summaries['d30001t'])
# print(cluster_summaries)

# %%

alldata = {'cluster_ids': clusters_ids, 'docs_ids': docs_ids, 'summaries_clusters': cluster_summaries, 'data': df}
# with open('../../task_12.pkl', 'wb') as handle:
#    pickle.dump(alldata, handle, protocol=pickle.HIGHEST_PROTOCOL)

# %%


from src.helpers import Duc

print(clusters_ids)
d = Duc(clusters_ids, docs_ids, cluster_summaries, df)
with open('../../out/task_12.pkl', 'wb') as handle:
    pickle.dump(d, handle, protocol=pickle.HIGHEST_PROTOCOL)
