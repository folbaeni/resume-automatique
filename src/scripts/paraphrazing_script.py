import sys
sys.path.append('../..')
from src.paraphraser import *
import pandas as pd
import numpy as np
import nltk
import tqdm
import transformers

best_summaries = np.load("../../out/best_generated_summaries_excel.npy")

T5_summaries = []

pegasus_summaries = []

cache_dir='/tmp'

tokenizerPegasus=transformers.PegasusTokenizer.from_pretrained('tuner007/pegasus_paraphrase', cache_dir=cache_dir)
modelPegasus=transformers.PegasusForConditionalGeneration.from_pretrained('tuner007/pegasus_paraphrase', cache_dir=cache_dir).to("cuda")
tokenizerT5=transformers.AutoTokenizer.from_pretrained("Vamsi/T5_Paraphrase_Paws", cache_dir=cache_dir)
modelT5=transformers.AutoModelForSeq2SeqLM.from_pretrained("Vamsi/T5_Paraphrase_Paws", cache_dir=cache_dir).cuda()
for summary in tqdm.tqdm(best_summaries):
    try:
        list_of_sentences = nltk.tokenize.sent_tokenize(summary)
    except Exception as e:
        print(e)
        nltk.download('punkt')
        list_of_sentences = nltk.tokenize.sent_tokenize(summary)
    
    T5_summaries.append(paraphrase_list_of_sentencesT5(list_of_sentences,tokenizer=tokenizerT5,model=modelT5))
    pegasus_summaries.append(paraphrase_list_of_sentencesPegasus(list_of_sentences,tokenizer=tokenizerPegasus,model=modelPegasus))
    
T5_summaries = np.array(T5_summaries)
pegasus_summaries = np.array(pegasus_summaries)

np.save("../../out/pegasus_summaries_excel", pegasus_summaries)
np.save("../../out/T5_summaries_excel", T5_summaries)
