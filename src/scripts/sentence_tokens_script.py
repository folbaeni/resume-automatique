import sys

import tqdm
from sentence_transformers import SentenceTransformer

sys.path.append('..')

f = open('../../out/mpnet.pkl', 'wb')

model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2', cache_folder='/tmp/trans')
# dataset = pd.read_excel('../../dataset.xlsx')
# content = parse_list_of_texts(dataset['content'])

import pickle

d = pickle.load(open('../../out/task_12.pkl', 'rb'))
content = d.clusters_corpuses()

embeddingsList = []

# print(len(content))
for sentences in tqdm.tqdm(content):
    embeddings = model.encode(sentences)
    # print(embeddings.shape)
    embeddingsList.append(embeddings)

# np.save('../../out/mpnet_excel',embeddingsList,dtype=object)
pickle.dump(embeddingsList, f)
f.close()
