import torch
from transformers import PegasusForConditionalGeneration, PegasusTokenizer
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM
from typing import List


def paraphrase_list_of_sentencesPegasus(sentences: List[str], cache_dir: str = "/tmp/peg", returnList: bool = False,tokenizer=None,model=None):
    # ref: https://analyticsindiamag.com/how-to-paraphrase-text-using-pegasus-transformer/

    torch_device = 'cuda'
    
    if tokenizer is None:
        tokenizer = PegasusTokenizer.from_pretrained('tuner007/pegasus_paraphrase', cache_dir=cache_dir)
    if model is None:
        model = PegasusForConditionalGeneration.from_pretrained('tuner007/pegasus_paraphrase', cache_dir=cache_dir).to(torch_device)

    def get_response(input_text, num_return_sequences):
        batch = tokenizer.prepare_seq2seq_batch([input_text], truncation=True, padding='longest', max_length=60,
                                                return_tensors="pt").to(torch_device)
        translated = model.generate(**batch, max_length=60, num_beams=10, num_return_sequences=num_return_sequences,
                                    temperature=1.5)
        tgt_text = tokenizer.batch_decode(translated, skip_special_tokens=True)
        return tgt_text

    newtext = []
    for text in sentences:
        newtext.append(get_response(text, 1)[0])
    if returnList:
        return newtext
    else:
        return " ".join(newtext)

def paraphrase_list_of_sentencesT5(sentences: List[str], cache_dir: str = "/tmp/t5", returnList: bool = False,tokenizer=None,model=None):
    #ref : https://huggingface.co/Vamsi/T5_Paraphrase_Paws
    if tokenizer is None:
        tokenizer = AutoTokenizer.from_pretrained("Vamsi/T5_Paraphrase_Paws", cache_dir=cache_dir)
    if model is None:
        model = AutoModelForSeq2SeqLM.from_pretrained("Vamsi/T5_Paraphrase_Paws", cache_dir=cache_dir).cuda()
    newText=[]

    for sentence in sentences:
        text =  "paraphrase: " + sentence + " </s>"
        encoding = tokenizer.encode_plus(text,pad_to_max_length=True, return_tensors="pt")
        input_ids, attention_masks = encoding["input_ids"].to("cuda"), encoding["attention_mask"].to("cuda")


        output = model.generate(
            input_ids=input_ids, attention_mask=attention_masks,
            max_length=256,
            do_sample=True,
            top_k=120,
            top_p=0.95,
            early_stopping=True,
            num_return_sequences=1
        )[0]

        
        newText.append(tokenizer.decode(output, skip_special_tokens=True,clean_up_tokenization_spaces=True))
    if returnList:
        return newText
    else:
        return " ".join(newText)
