import numpy as np
from sklearn.preprocessing import normalize

"""
def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)
"""


def multidoc_clustering(centres, listOfXs, norm='l1'):
    return [doc_clustering(centres, X, norm=norm) for X in listOfXs]


"""
    separators = np.cumsum([0] + [len(t) for t in docs])

    i = 0
    res = []
    for s,e in pairwise(separators):
        res.append(clustering(centres, X[s:e]) )
        print('document:', i, res[-1])
        i += 1
    return res
"""


def doc_clustering(centres, X, text=None, norm='l1'):
    res = []
    if norm is not None:
        Xnormalized = normalize(X, axis=1, norm=norm)
    else:
        Xnormalized = X

    for centre in centres:
        res.append(np.argmax((Xnormalized @ centre.reshape(-1, 1))))
        # res.append(np.argmin([np.sum(np.abs(centre -  ve)) for ve in Xnormalized]))

    res = np.array(res).astype(int)

    assert len(centres) == len(res), 'bug in doc_clustering'

    if text is not None:
        print(np.array(text)[np.sort(res)])
    return res


def textRank(X: np.array, d: float = 0.85, allowed_error_rate: float = 10e-8, max_iter: int = 1000,
             nb_clusters: int = -1, use_log:bool = True) -> np.array:
    eps = 1e-10

    # array such that for all i sentence_lengths[i] contains the length of the i-th sentence
    sentence_lengths = np.sum(X, axis=1)

    nb_sentences = len(sentence_lengths)

    sentence_lengths = sentence_lengths.reshape(nb_sentences, 1)

    # the denominator of the simmilarity measure given in the paper
    normalization_factor = sentence_lengths @ sentence_lengths.T
    # we add eps=1e-10 to the elements before calculating the logarithm to avoid having to calculate the log of 0
    if use_log:
        normalization_factor=np.log(normalization_factor+eps)

    # the coordiante i,j of this matrix contains the simmilarity measure between the ith and jth sentence
    # the simmilarity measure as defined in the TextRank Paper
    adjacency_matrix = np.array((X @ X.T) / normalization_factor)

    # the i-th element of this array gives the sum of adjacency_matrix[i,k] for all k!=i
    sum_of_weights = np.sum(adjacency_matrix, axis=0)+eps

    # We normalize the adjacency matrix such that in every line i the sum of adjacency_matrix[i,k] for all k!=1 is 1
    adjacency_matrix = adjacency_matrix / sum_of_weights

    # we initalize the scores randomly
    score = np.random.rand(nb_sentences)

    current_iter = 0
    while True:
        current_iter += 1

        # we keep track if the score of at least one vertex has changed in this iteration
        score_has_changed = False

        for i in range(nb_sentences):
            # we calculate the new score of the i-th sentence as described in the paper
            newScore = (adjacency_matrix[i] * score)
            newScore = np.sum(newScore)
            newScore = (1 - d) + d * newScore
            if np.abs(score[i] - newScore) > allowed_error_rate:
                score_has_changed = True
            score[i] = newScore

        if current_iter > max_iter or not score_has_changed:
            break

    sorted_indexes = np.argsort(-score)

    if nb_clusters > 0:
        return np.sort(sorted_indexes[:nb_clusters])
    else:
        return sorted_indexes
