import string
import unicodedata
import re
import nltk
import sklearn
import glob
import numpy as np
import os

"""
EXTRACTION FROM FILES
"""


def parse_text(text: str) -> str:
    try:
        res = nltk.tokenize.sent_tokenize(text)
    except Exception as e:
        print(e)
        nltk.download('punkt')
        res = nltk.tokenize.sent_tokenize(text)
    return res


def parse_list_of_texts(list_texts):
    return [parse_text(text) for text in list_texts]


def parse_file(filename: str) -> str:
    f = open(filename, 'r')
    res = parse_text(f.read())
    f.close()
    return res


def parse_folder(path):
    # return list of doc
    return [parse_file(filename) for filename in glob.glob(os.path.join(path, '*.txt'))]



"""
INITIAL PREPROCESSING
"""


def preprocess_sentence(text, ponc=True, specials=True, lower=True, numbers=True):
    punc = string.punctuation  # recupération de la ponctuation
    punc += '\n\r\t'
    if ponc:
        text = text.translate(str.maketrans(punc, ' ' * len(punc)))

    if specials:
        text = unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode("utf-8")

    if lower:
        text = text.lower()

    if numbers:
        text = re.sub('[0-9]+', '', text)
    return text


def preprocess_corpus(sentences, ponc=True, specials=True, lower=True, numbers=True):
    # sentence is string 
    return [preprocess_sentence(s, ponc=ponc, specials=specials, lower=lower, numbers=numbers) for s in sentences]


def preprocess_docs(corpuses, ponc=True, specials=True, lower=True, numbers=True):
    # corpus is list of sentences
    # print(corpuses)
    return [preprocess_corpus(c, ponc=ponc, specials=specials, lower=lower, numbers=numbers) for c in corpuses]


"""
STEMMING
"""


def stem_sentence(text,remove_stopwords=True):
    if remove_stopwords:
        try:
            stop = nltk.corpus.stopwords.words('english')
        except Exception as e:
            print(e)
            nltk.download('stopwords')
            stop = nltk.corpus.stopwords.words('english')
    else:
        stop=None

    stemmer = nltk.stem.snowball.EnglishStemmer()
    analyzer = sklearn.feature_extraction.text.CountVectorizer(stop_words=stop,
                                                               lowercase=False).build_analyzer()

    return ' '.join([stemmer.stem(w) for w in analyzer(text)])


def stem_corpus(sentences,remove_stopwords=True):
    return [stem_sentence(s,remove_stopwords) for s in sentences]


def stem_docs(corpuses,remove_stopwords=True):
    return [stem_corpus(c,remove_stopwords) for c in corpuses]


"""
VECTORIZING
"""


def vectorize_vocabulary(sentences, min_df=0, max_df=1.0, ngram_range=(1, 1), max_features=None):
    vectorizer = sklearn.feature_extraction.text.CountVectorizer(min_df=min_df,
                                                                 max_df=max_df,
                                                                 ngram_range=ngram_range,
                                                                 lowercase=False,
                                                                 max_features=max_features)

    X = vectorizer.fit_transform(sentences)
    return vectorizer, X


def vectorize_vocabulary_multidoc(corpuses, min_df=0, max_df=1.0, ngram_range=(1, 1), max_features=None):
    sentences_merge = np.concatenate(tuple(corpuses))
    return vectorize_vocabulary(sentences_merge, min_df=min_df, max_df=max_df, ngram_range=ngram_range,
                                max_features=max_features)

