#!/usr/bin/perl -w
@sysid=();
@score=();
@ciL=();
@ciU=();
while(defined($line=<STDIN>)) {
    chomp($line);
    @tmp=split(/\s+/,$line);
    push(@sysid,$tmp[0]);
    push(@score,$tmp[3]);
    chop($tmp[$#tmp]);
    $ci=sprintf("%7.5f",$tmp[3]-$tmp[$#tmp]);
    push(@ciL,$ci);
    $ci=sprintf("%7.5f",$tmp[3]+$tmp[$#tmp]);
    push(@ciU,$ci);
    $measure=$tmp[1];
}
print "SYSID\t$measure\t95\% CI Lower\t95\% CI Upper\n";
for($i=0;$i<=$#sysid;$i++) {
    print "$sysid[$i]\t$score[$i]\t$ciL[$i]\t$ciU[$i]\n";
}
