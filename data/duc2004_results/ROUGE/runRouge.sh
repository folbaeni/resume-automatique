cd /home/barney1/duc/duc2004/software

/home/barney1/duc/ROUGE/rouge -a -c 95 -b 75   -m -n 4  -w 1.2 t1.rouge.in > t1.rouge.out
/home/barney1/duc/ROUGE/rouge -a -c 95 -b 665  -m -n 4  -w 1.2 t2.rouge.in > t2.rouge.out
/home/barney1/duc/ROUGE/rouge -a -c 95 -b 75   -m -n 4  -w 1.2 t3.rouge.in > t3.rouge.out
/home/barney1/duc/ROUGE/rouge -a -c 95 -b 665  -m -n 4  -w 1.2 t4.rouge.in > t4.rouge.out
/home/barney1/duc/ROUGE/rouge -a -c 95 -b 665  -m -n 4  -w 1.2 t5.rouge.in > t5.rouge.out

grep "ROUGE-1 Average"     t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >  t1.rouge.out.tab
grep "ROUGE-2 Average"     t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t1.rouge.out.tab
grep "ROUGE-3 Average"     t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t1.rouge.out.tab
grep "ROUGE-4 Average"     t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t1.rouge.out.tab
grep "ROUGE-L Average"     t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t1.rouge.out.tab
grep "ROUGE-W-1.2 Average" t1.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t1.rouge.out.tab

grep "ROUGE-1 Average"     t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >  t2.rouge.out.tab
grep "ROUGE-2 Average"     t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t2.rouge.out.tab
grep "ROUGE-3 Average"     t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t2.rouge.out.tab
grep "ROUGE-4 Average"     t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t2.rouge.out.tab
grep "ROUGE-L Average"     t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t2.rouge.out.tab
grep "ROUGE-W-1.2 Average" t2.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t2.rouge.out.tab

grep "ROUGE-1 Average"     t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >  t3.rouge.out.tab
grep "ROUGE-2 Average"     t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t3.rouge.out.tab
grep "ROUGE-3 Average"     t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t3.rouge.out.tab
grep "ROUGE-4 Average"     t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t3.rouge.out.tab
grep "ROUGE-L Average"     t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t3.rouge.out.tab
grep "ROUGE-W-1.2 Average" t3.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t3.rouge.out.tab

grep "ROUGE-1 Average"     t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >  t4.rouge.out.tab
grep "ROUGE-2 Average"     t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t4.rouge.out.tab
grep "ROUGE-3 Average"     t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t4.rouge.out.tab
grep "ROUGE-4 Average"     t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t4.rouge.out.tab
grep "ROUGE-L Average"     t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t4.rouge.out.tab
grep "ROUGE-W-1.2 Average" t4.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t4.rouge.out.tab

grep "ROUGE-1 Average"     t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >  t5.rouge.out.tab
grep "ROUGE-2 Average"     t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t5.rouge.out.tab
grep "ROUGE-3 Average"     t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t5.rouge.out.tab
grep "ROUGE-4 Average"     t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t5.rouge.out.tab
grep "ROUGE-L Average"     t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t5.rouge.out.tab
grep "ROUGE-W-1.2 Average" t5.rouge.out | sort -k 4 -n -r | mkCITable.pl >> t5.rouge.out.tab
